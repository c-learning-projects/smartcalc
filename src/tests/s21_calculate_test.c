#include <check.h>
#include "../s21_calculate.h"

START_TEST(test1) {
    char res_str[STACK_MAX_SIZE];
    char exit_str[STACK_MAX_SIZE][STACK_MAX_SIZE];
    int num_of_items = 0, tr_res;
    char* str = {"1+16/2^3*(10-7)"};
    tr_res = translate(str, exit_str, &num_of_items);
    double res = 7;
    ck_assert_double_eq(res, calc(exit_str, &num_of_items));
}

START_TEST(test2) {
    char res_str[STACK_MAX_SIZE];
    char exit_str[STACK_MAX_SIZE][STACK_MAX_SIZE];
    int num_of_items = 0, tr_res;
    char str[255] = {" 1 + 16 / 2 ^ 3 * ( 10 - 7 ) "};
    tr_res = translate(str, exit_str, &num_of_items);
    double res = 7;
    ck_assert_double_eq(res, calc(exit_str, &num_of_items));
}

START_TEST(test3) {
    char res_str[STACK_MAX_SIZE];
    char exit_str[STACK_MAX_SIZE][STACK_MAX_SIZE];
    int num_of_items = 0, tr_res;
    char str[255] = {"sin(1)cos(1)tan(1)log(1)acos(1)atan(1)√4ln(1)"};
    tr_res = translate(str, exit_str, &num_of_items);
    double res = 0;
    ck_assert_double_eq(res, calc(exit_str, &num_of_items));
}

START_TEST(test4) {
    char res_str[STACK_MAX_SIZE];
    char exit_str[STACK_MAX_SIZE][STACK_MAX_SIZE];
    int num_of_items = 0, tr_res;
    char str[255] = {"sin((1)"};
    tr_res = translate(str, exit_str, &num_of_items);
    double res = 0;
    ck_assert_double_eq(res, tr_res);
}

START_TEST(test5) {
    char res_str[STACK_MAX_SIZE];
    char exit_str[STACK_MAX_SIZE][STACK_MAX_SIZE];
    int num_of_items = 0, tr_res;
    char str[255] = {"sin(x)"};
    tr_res = translate(str, exit_str, &num_of_items);
    double res = 0;
    ck_assert_double_eq(res, strcmp("x", exit_str[0]));
    ck_assert_double_eq(res, strcmp("s", exit_str[1]));
}

START_TEST(test6) {
    char str[255] = {"2*2"};
    double res = 4;
    ck_assert_double_eq(res, calculate(str));
}

Suite *new_suite_create(void) {
    Suite *suite = suite_create("Core");
    TCase *tcase_core = tcase_create("Core of new");
    tcase_add_test(tcase_core, test1);
    tcase_add_test(tcase_core, test2);
    tcase_add_test(tcase_core, test3);
    tcase_add_test(tcase_core, test4);
    tcase_add_test(tcase_core, test5);
    tcase_add_test(tcase_core, test6);
    suite_add_tcase(suite, tcase_core);
    return suite;
}

int main(void) {
    Suite *suite = new_suite_create();
    SRunner *suite_runner = srunner_create(suite);
    srunner_run_all(suite_runner, CK_NORMAL);
    int failed_count = srunner_ntests_failed(suite_runner);
    srunner_free(suite_runner);
    return failed_count;
}
