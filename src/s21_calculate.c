#include "s21_calculate.h"

void push(Stack_t *stack, const double value) {
    if (stack->size >= STACK_MAX_SIZE) {
        exit(STACK_OVERFLOW);
    }
    stack->data[stack->size] = value;
    stack->size++;
}

double pop(Stack_t *stack) {
    if (stack->size == 0) {
        exit(STACK_UNDERFLOW);
    }
    stack->size--;
    return stack->data[stack->size];
}

double peek(const Stack_t *stack) {
    if (stack->size <= 0) {
        exit(STACK_UNDERFLOW);
    }
    return stack->data[stack->size - 1];
}

int prioritet(char ch1, char ch2) {
    int val1 = -1, val2 = -1;
    if (ch1 == '^') val1 = 0;
    else if (ch1 == '*' || ch1 == '/') val1 = 1;
    else if (ch1 == '+' || ch1 == '-') val1 = 2;
    else if (ch1 == '(' || ch1 == ')') val1 = 3;
    if (ch2 == '^') val2 = 0;
    else if (ch2 == '*' || ch2 == '/') val2 = 1;
    else if (ch2 == '+' || ch2 == '-') val2 = 2;
    else if (ch2 == '(' || ch2 == ')') val2 = 3;
    return val1 <= val2;
}

int detector(char ch) {
    int res = -1;
    char* op = {"+-*^%/"};
    if (ch >= '0' && ch <= '9') res = ITS_NUM;
    else if (ch >= 'a' && ch <= 'z') res = ITS_FUN;
    else if (strchr(op, ch) != NULL) res = ITS_OP;
    else if (ch == ')') res = ITS_CLOSE;
    else if (ch == '(') res = ITS_OPEN;
    return res;
}

void imove(char* str, int* i, int* str_len) {
    while (((detector(str[*i]) == ITS_NUM) || (str[*i] == '.')) && (*i < *str_len)) {
        (*i)++;
    }
    (*i)--;
}

void insert_ch(char ch, int n, char* str) {
    int i = strlen(str);
    str[i + 1] = '\0';
    while (i > n) {
        str[i] = str[i - 1];
        i--;
    }
    str[i] = ch;
}

void delete_ch(int n, char* str) {
    int i = strlen(str);
    while (n < i) {
        str[n] = str[n + 1];
        n++;
    }
}

void delete_chs(int n, int c, char* str) {
    int i = 0;
    while (i < c) {
        delete_ch(n, str);
        i++;
    }
}

void replace_chs_on_ch(int n, int c, char* str, char ch) {
    delete_chs(n, c, str);
    insert_ch(ch, n, str);
}

void delete_spaces(char* str) {
    int str_len = strlen(str);
    int  i = 0;
    while (i < str_len) {
        if (str[i] == ' ') delete_ch(i, str);
        else
            i++;
    }
}

void fun_traslate(char* str) {
    char funcs[][5] = {"acos", "asin", "atan", "√", "log", "ln", "cos", "sin", "tan"};
    int i = 0, n = 0;
    char* temp_adr;
    while (i < 9) {
        while ((temp_adr = strstr(str, funcs[i])) != NULL) {
            if (!strcmp(funcs[i], "cos") || !strcmp(funcs[i], "sin")
                || !strcmp(funcs[i], "tan") || !strcmp(funcs[i], "log")) {
                n = temp_adr - str + 1;
                delete_chs(n, 2, str);
            } else if (!strcmp(funcs[i], "acos")) {
                n = temp_adr - str;
                replace_chs_on_ch(n, 4, str, 'k');
            } else if (!strcmp(funcs[i], "asin")) {
                n = temp_adr - str + 1;
                delete_chs(n, 3, str);
            } else if (!strcmp(funcs[i], "atan")) {
                n = temp_adr - str;
                replace_chs_on_ch(n, 4, str, 'n');
            } else if (!strcmp(funcs[i], "√")) {
                n = temp_adr - str;
                replace_chs_on_ch(n, 3, str, 'q');
            } else if (!strcmp(funcs[i], "ln")) {
                n = temp_adr - str;
                replace_chs_on_ch(n, 2, str, 'g');
            }
        }
        i++;
    }
}

int pre_translate(char* str) {
    int res = 1;
    if (detector(str[0]) == ITS_OP) insert_ch('0', 0, str);
    delete_spaces(str);
    if (str[0] == ',') res = 0;
    if (str[strlen(str) - 1] == '.' || detector(str[strlen(str) - 1]) == ITS_OP) res = 0;
    for (int i = 0; i < (int)strlen(str) && res == 1; i++) {
        if (str[i] == '\n') res = 0;
        if (str[i] == '(' && detector(str[i + 1]) == ITS_OP) insert_ch('0', i + 1, str);
        if (str[i] == '(' && i > 0 && detector(str[i - 1]) == ITS_NUM) insert_ch('*', i, str);
        if (i > 0 && detector(str[i]) == ITS_FUN && detector(str[i - 1]) == ITS_NUM) insert_ch('*', i, str);
        if (str[i] == ')' && detector(str[i + 1]) == ITS_NUM) insert_ch('*', i + 1, str);
        if (str[i] == 'x' && detector(str[i + 1]) == ITS_NUM) insert_ch('*', i + 1, str);
        if (str[i] == 'x' && str[i + 1] == 'x') insert_ch('*', i + 1, str);
        if (i > 0 && detector(str[i]) == ITS_OP && detector(str[i - 1]) == ITS_OP) res = 0;
        if (i > 0 && str[i] == ')' && str[i - 1] == '(') res = 0;
        if (i > 0 && str[i] == '.' && detector(str[i - 1]) != ITS_NUM)  res = 0;
        if (i > 0 && detector(str[i]) == ITS_OP && detector(str[i - 1]) == ITS_OP) res = 0;
        if (i > 0 && detector(str[i]) == ITS_FUN && str[i - 1] == ')') insert_ch('*', i, str);
    }
    return res;
}

int sub_sub_translate(Stack_t* stack, char exit_str[STACK_MAX_SIZE][STACK_MAX_SIZE], int* num_of_items) {
    int res = 1;
    if (peek(stack) == '(' || stack->size == 0) {
        res = 0;
    } else {
        snprintf(exit_str[*num_of_items], sizeof(exit_str[*num_of_items]), "%c", (int)pop(stack));
        (*num_of_items)++;
    }
    return res;
}

int sub_traslate(char ch, Stack_t* stack, char exit_str[STACK_MAX_SIZE][STACK_MAX_SIZE], int* num_of_items) {
    int res = 1;
    if (ch == ')') {
        while (stack->size > 0 && peek(stack) != '(') {
            sub_sub_translate(stack, exit_str, num_of_items);
        }
        if (stack->size != 0) {
            pop(stack);
        } else {
            res = 0;
        }
    } else {
        res = sub_sub_translate(stack, exit_str, num_of_items);
        push(stack, ch);
    }
    return res;
}

int translate(char* str, char exit_str[STACK_MAX_SIZE][STACK_MAX_SIZE], int* num_of_items) {
    setlocale(LC_ALL, "en_US.utf8");
    int res = 1;
    if (strlen(str) < 1) res = 0;
    if (strlen(str) > 0 && detector(str[strlen(str) - 1]) == ITS_OP) res = 0;
    if (strstr(str, "error") != NULL) res = 0;
    Stack_t stack; stack.size = 0; double d_temp = 0;
    int det_res = -1;
    fun_traslate(str);
    res = pre_translate(str);
    int str_len = strlen(str);
    for (int i = 0; i < str_len && res == 1; i++) {
        det_res = detector(str[i]);
        if (det_res == ITS_NUM) {
            sscanf(str + i, "%lf", &d_temp);
            snprintf(exit_str[*num_of_items], STACK_MAX_SIZE, "%.8lf", d_temp);
            (*num_of_items)++;
            imove(str, &i, &str_len);
        } else if (str[i] == 'x') {
            exit_str[*num_of_items][0] = str[i];
            exit_str[*num_of_items][1] = '\0';
            (*num_of_items)++;
        } else if (det_res == ITS_FUN || det_res == ITS_OPEN) {
            push(&stack, str[i]);
        } else if (det_res == ITS_CLOSE) {
            res = sub_traslate(str[i], &stack, exit_str, num_of_items);
        } else if (det_res == ITS_OP) {
            while (stack.size > 0 && (detector(peek(&stack)) == ITS_FUN || prioritet(peek(&stack), str[i]))) {
                sub_sub_translate(&stack, exit_str, num_of_items);
            }
            push(&stack, str[i]);
        }
    }
    while (stack.size > 0 && res == 1) {
        res = sub_sub_translate(&stack, exit_str, num_of_items);
    }
    return res;
}

int str_to_stack(char* str, Stack_t* stack) {
    int res = 1;
    double tmp = 0;
    if (sscanf(str, "%lf", &tmp)) push(stack, tmp);
    else
        res = 0;
    return res;
}

double calc(char str[STACK_MAX_SIZE][STACK_MAX_SIZE], int* num_of_items) {
    double res = 1; int i = 0; Stack_t stack; stack.size = 0; int det_res = -1;
    while (i < *num_of_items) {
        det_res = detector(str[i][0]);
        if (det_res == ITS_OP && detector(str[i][1]) == ITS_NUM) det_res = ITS_NUM;
        if (det_res == ITS_NUM) str_to_stack(str[i], &stack);
        if (det_res == ITS_FUN || det_res == ITS_OP) {
            if (str[i][0] == '+') {
                res = pop(&stack) + pop(&stack);
            } else if (str[i][0] == '-') {
                res = pop(&stack); res = pop(&stack) - res;
            } else if (str[i][0] == '*') {
                res = pop(&stack) * pop(&stack);
            } else if (str[i][0] == '/') {
                res = pop(&stack); res = pop(&stack) / res;
            } else if (str[i][0] == '^') {
                res = pop(&stack); res = pow(pop(&stack), res);
            } else if (str[i][0] == '%') {
                res = pop(&stack); res = fmod(pop(&stack), res);
            } else if (str[i][0] == 'c') {
                res = cos(pop(&stack));
            } else if (str[i][0] == 's') {
                res = sin(pop(&stack));
            } else if (str[i][0] == 't') {
                res = tan(pop(&stack));
            } else if (str[i][0] == 'k') {
                res = acos(pop(&stack));
            } else if (str[i][0] == 'a') {
                res = asin(pop(&stack));
            } else if (str[i][0] == 'n') {
                res = atan(pop(&stack));
            } else if (str[i][0] == 'q') {
                res = sqrt(pop(&stack));
            } else if (str[i][0] == 'l') {
                res = log10(pop(&stack));
            } else if (str[i][0] == 'g') {
                res = log(pop(&stack));
            }
            push(&stack, res);
        }
        i++;
    }
    return pop(&stack);
}

double calculate(char* str) {
    char exit_str[STACK_MAX_SIZE][STACK_MAX_SIZE];
    int num_of_items = 0;
    translate(str, exit_str, &num_of_items);
    return calc(exit_str, &num_of_items);
}
