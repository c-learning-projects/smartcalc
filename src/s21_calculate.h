#ifndef SRC_S21_CALCULATE_H_
#define SRC_S21_CALCULATE_H_

#include <stdio.h>
#include "math.h"
#include <string.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>

#define STACK_MAX_SIZE 510
#define STACK_OVERFLOW  -100
#define STACK_UNDERFLOW -101

#define ITS_NUM 0
#define ITS_FUN 1
#define ITS_OP 2
#define ITS_CLOSE 3
#define ITS_OPEN 4

typedef struct Stack_tag {
    double data[STACK_MAX_SIZE];
    size_t size;
} Stack_t;

double calc(char str[STACK_MAX_SIZE][STACK_MAX_SIZE], int* num_of_items);
int translate(char* str, char exit_str[STACK_MAX_SIZE][STACK_MAX_SIZE], int* num_of_items);
double calculate(char* str);
#endif  // SRC_S21_CALCULATE_H_
