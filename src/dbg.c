#include "s21_calculate.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>
#include <cairo.h>
#include <locale.h>
#include <time.h>
// #include <time.h>

GtkBuilder *ui_builder;
G_MODULE_EXPORT void onExit(GtkWidget *widget) {
    gtk_main_quit();
}

void to_textbox(GtkWidget *widget, GtkWidget *textbox, char* str) {
        GtkTextBuffer *buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textbox));
        GtkTextIter iter;
        gtk_text_buffer_get_end_iter(buffer, &iter);
        gtk_text_buffer_insert(buffer, &iter, str, -1);
}

G_MODULE_EXPORT void hide_form2_clicked_cb(GtkWidget *widget, GtkWidget *form2) {
    gtk_widget_hide(form2);
}

G_MODULE_EXPORT void type_0(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "0");
}

G_MODULE_EXPORT void type_1(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "1");
}

G_MODULE_EXPORT void type_2(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "2");
}

G_MODULE_EXPORT void type_3(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "3");
}

G_MODULE_EXPORT void type_4(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "4");
}

G_MODULE_EXPORT void type_5(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "5");
}

G_MODULE_EXPORT void type_6(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "6");
}

G_MODULE_EXPORT void type_7(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "7");
}

G_MODULE_EXPORT void type_8(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "8");
}

G_MODULE_EXPORT void type_9(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "9");
}

G_MODULE_EXPORT void type_40(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "(");
}

G_MODULE_EXPORT void type_41(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, ")");
}

G_MODULE_EXPORT void type_sin(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "sin(");
}

G_MODULE_EXPORT void type_cos(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "cos(");
}

G_MODULE_EXPORT void type_tan(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "tan(");
}

G_MODULE_EXPORT void type_asin(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "asin(");
}

G_MODULE_EXPORT void type_acos(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "acos(");
}

G_MODULE_EXPORT void type_atan(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "atan(");
}

G_MODULE_EXPORT void type_ln(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "ln(");
}

G_MODULE_EXPORT void type_log(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "log(");
}

G_MODULE_EXPORT void type_x(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "x");
}

G_MODULE_EXPORT void type_sum(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "+");
}

G_MODULE_EXPORT void type_sub(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "-");
}

G_MODULE_EXPORT void type_pow(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "^");
}

G_MODULE_EXPORT void type_div(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "/");
}

G_MODULE_EXPORT void type_mul(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "*");
}

G_MODULE_EXPORT void type_mod(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "%");
}

G_MODULE_EXPORT void type_sqrt(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, "√(");
}

G_MODULE_EXPORT void ac(GtkWidget *widget, GtkWidget *textbox) {
    gtk_text_buffer_set_text(gtk_text_view_get_buffer(GTK_TEXT_VIEW(textbox)), "", -1);
}

G_MODULE_EXPORT void type_dot(GtkWidget *widget, GtkWidget *textbox) {
    to_textbox(widget, textbox, ".");
}

G_MODULE_EXPORT void bs(GtkWidget *widget, GtkWidget *textbox) {
    GtkTextBuffer *buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textbox));
    GtkTextIter start, end;
    gtk_text_buffer_get_start_iter(buffer, &start);
    gtk_text_buffer_get_end_iter(buffer, &end);
    char* text = gtk_text_buffer_get_text(buffer, &start, &end, -1);
    if (strlen(text) > 0) text[strlen(text) - 1] = '\0';
    while (strlen(text) > 0 && text[strlen(text) - 1] > 'a' && text[strlen(text) - 1] < 'z'
            && text[strlen(text) - 1] != 'x') {
        text[strlen(text) - 1] = '\0';
    }
    gtk_text_buffer_set_text(gtk_text_view_get_buffer(GTK_TEXT_VIEW(textbox)), text, -1);
}

void x_detector(char exit_str[STACK_MAX_SIZE][STACK_MAX_SIZE], int *num_of_items, int xi[255], int *xn) {
    int i = 0, j = 0;
    while (i < *num_of_items) {
        if (strchr(exit_str[i], 'x')) {
            xi[j] = i;
            j++;
        }
        i++;
    }
    (*xn) = j;
}

void insert_x(char exit_str[STACK_MAX_SIZE][STACK_MAX_SIZE], int xi[255], double x, int *xn) {
    int i = 0;
    while (i < *xn) {
        snprintf(exit_str[xi[i]], STACK_MAX_SIZE, "%.8lf", x);
        i++;
    }
}

const char* get_text_from_widget(char* name) {
    GtkEntry* ent = GTK_ENTRY(gtk_builder_get_object(ui_builder, name));
    return gtk_entry_get_text(ent);
}

int get_widget_width(char* name) {
    GtkWidget* ent = GTK_WIDGET(gtk_builder_get_object(ui_builder, name));
    return gtk_widget_get_allocated_width(ent);
}

int get_widget_height(char* name) {
    GtkWidget* ent = GTK_WIDGET(gtk_builder_get_object(ui_builder, name));
    return gtk_widget_get_allocated_height(ent);
}

int text_check(const char* text) {
    int res = 1, n = strlen(text), i = 0;
    while (res == 1 && i < n) {
        if ((text[i] < '0' || text[i] > '9') && (text[i]) != '-') res = 0;
        i++;
    }
    return res;
}

int get_xyminmax(int* xmin, int* xmax, int* ymin, int* ymax) {
    int res = 1;
    if (text_check(get_text_from_widget("xmin")) && text_check(get_text_from_widget("xmax")) &&
        text_check(get_text_from_widget("ymin")) && text_check(get_text_from_widget("ymax"))) {
            sscanf(get_text_from_widget("xmin"), "%d", xmin);
            sscanf(get_text_from_widget("xmax"), "%d", xmax);
            sscanf(get_text_from_widget("ymin"), "%d", ymin);
            sscanf(get_text_from_widget("ymax"), "%d", ymax);
    } else {
        res = 0;
    }
    if (*xmax <= *xmin || *ymax <= *ymin) res = 0;
    return res;
}

void make_points(char exit_str[STACK_MAX_SIZE][STACK_MAX_SIZE], int *num_of_items, double step, double** xp,
                    double** yp, int xmin, int xmax, int *xp_size) {
    *xp_size = (xmax - xmin) / step + 1.0;
    *xp = realloc(*xp, *xp_size * sizeof(double));
    *yp = realloc(*yp, *xp_size * sizeof(double));
    int xn = 0; double x = xmin, y = 0;
    int xi[255];
    x_detector(exit_str, num_of_items, xi, &xn);
    for (int i = 0; i < *xp_size; i++, x += step) {
        insert_x(exit_str, xi, x, &xn);
        y = calc(exit_str, num_of_items);
        if (!isnan(y) && !isinf(y)) {
            (*xp)[i] = x;
            (*yp)[i] = y;
        } else {
            (*xp)[i] = NAN;
            (*yp)[i] = NAN;
        }
    }
}

void fun_draw(cairo_t *cr, double* xp, double* yp, int xp_size, double xmin, double xmax, double ymin,
                double ymax, double area_h, double area_w, double xk, double yk) {
    xmax = xmax;
    area_w = area_w;
    double x = 0, y = 0;
    int flag_to_move = 0;
    cairo_move_to(cr, -1, -1);
    for (int i = 0; i < xp_size; i++) {
        x = xp[i] * xk - xmin * xk;
        y = area_h - (yp[i] * yk - ymin * yk);
        if (!isnan(xp[i]) && yp[i] < ymax && yp[i] > ymin) {
            if (flag_to_move) {
                cairo_move_to(cr, x, y);
                flag_to_move = 0;
            } else {
                cairo_line_to(cr, x, y);
            }
        } else {
            flag_to_move = 1;
        }
    }
    cairo_set_line_width(cr, 2);
    cairo_set_source_rgb(cr, 0, 0, 1);
    cairo_stroke(cr);
}

void img_to_double(double x, double y, double xk, double xmin, double area_h, double yk, double ymin,
                    double* xp, double* yp) {
    *xp = (x + xmin * xk) / xk;
    *yp = (y - area_h - ymin * yk) / yk;
}

void show_number(cairo_t* cr, double x, double y, double num) {
    char text[255] = {'\0'};
    snprintf(text, sizeof(text), "%.2lf", num);
    cairo_set_line_width(cr, 1);
    cairo_set_font_size(cr, 12);
    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_move_to(cr, x, y - 2);
    cairo_show_text(cr, text);
}

void y_draw(cairo_t *cr, double ymin, double ymax, double area_h, double area_w, double yk,
            double* yp, int* yi, double* y_line) {
    double y = 0; int i = 0;
    if (ymax > 0 && ymin < 0) {
        y = area_h + ymin * yk;
    } else if (ymax > 0 && ymin >= 0) {
        y = area_h;
    } else {
        y = 0;
    }
    *y_line = y;
    cairo_move_to(cr, 0, y);
    cairo_line_to(cr, area_w, y);
    cairo_set_line_width(cr, 2);
    cairo_set_source_rgb(cr, 1, 0, 0);
    cairo_stroke(cr);
    double y_d = area_h / 8, temp_y = y;
    for (int yi = 0; temp_y < area_h; yi++) {
        temp_y = y + yi * y_d;
        cairo_move_to(cr, 0, temp_y);
        cairo_line_to(cr, area_w, temp_y);
        yp[i] = temp_y; i++;
    }
    temp_y = y;
    for (int yi = y; temp_y > 0; yi--) {
        temp_y = y + yi * y_d;
        cairo_move_to(cr, 0, temp_y);
        cairo_line_to(cr, area_w, temp_y);
        yp[i] = temp_y; i++;
    }
    *yi = i;
    cairo_set_line_width(cr, 0.75);
    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_stroke(cr);
}

void x_draw(cairo_t *cr, double xmin, double xmax, double area_h, double area_w,
            double xk, double* xp, int* xi, double* x_line) {
    double x = 0; int i = 0;
    if (xmax > 0 && xmin < 0) {
        x = - xmin * xk;
    } else if (xmax > 0 && xmin >= 0) {
        x = 0;
    } else {
        x = area_w;
    }
    *x_line = x;
    cairo_move_to(cr, x, 0);
    cairo_line_to(cr, x, area_h);
    cairo_set_line_width(cr, 2);
    cairo_set_source_rgb(cr, 1, 0, 0);
    cairo_stroke(cr);
    double x_d = area_h / 8, temp_x = x;
    for (int xi = 0; temp_x < area_w; xi++) {
        temp_x = x + xi * x_d;
        cairo_move_to(cr, temp_x, 0);
        cairo_line_to(cr, temp_x, area_h);
        xp[i] = temp_x; i++;
    }
    temp_x = x;
    for (int xi = x; temp_x > 0; xi--) {
        temp_x = x + xi * x_d;
        cairo_move_to(cr, temp_x, 0);
        cairo_line_to(cr, temp_x, area_h);
        xp[i] = temp_x; i++;
    }
    *xi = i;
    cairo_set_line_width(cr, 0.5);
    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_stroke(cr);
}

void xy_draw(cairo_t *cr, double xmin, double xmax, double ymin, double ymax,
                double area_h, double area_w, double xk, double yk) {
    double x_coord_arr[1000], y_coord_arr[1000], x_line = 0, y_line = 0, x_num = 0, y_num = 0; int xi, yi;
    y_draw(cr, ymin, ymax, area_h, area_w, yk, y_coord_arr, &yi, &y_line);
    x_draw(cr, xmin, xmax, area_h, area_w, xk, x_coord_arr, &xi, &x_line);
    if (x_line > area_w / 2) x_line -= 30;
    if (y_line < area_h / 2) y_line += 18;
    for (int i = 0; i < yi; i++) {
        img_to_double(x_line, y_coord_arr[i], xk, xmin, area_h, yk, ymin, &x_num, &y_num);
        show_number(cr, x_line, y_coord_arr[i], y_num);
    }
    for (int i = 0; i < xi; i++) {
        img_to_double(x_coord_arr[i], y_line, xk, xmin, area_h, yk, ymin, &x_num, &y_num);
        show_number(cr, x_coord_arr[i], y_line, x_num);
    }
}

void draw_area_clean(cairo_t *cr, double area_h, double area_w) {
    cairo_rectangle(cr, 0, 0, area_w, area_h);
    cairo_set_source_rgb(cr, 1, 1, 1);
    cairo_paint(cr);
    cairo_stroke(cr);
}

void myfree(void* link) {
    if (link != NULL) {
        free(link);
        link = NULL;
    }
}

void draw(char exit_str[STACK_MAX_SIZE][STACK_MAX_SIZE], int *num_of_items) {
    int res = 1; double step = 0;
    int xmin = 0, xmax = 0, ymin = 0, ymax = 0;
    res = get_xyminmax(&xmin, &xmax, &ymin, &ymax);
    sscanf(get_text_from_widget("step"), "%lf", &step);
    double area_h = get_widget_height("drawarea"), area_w = get_widget_width("drawarea");
    GtkWidget* drawarea = GTK_WIDGET(gtk_builder_get_object(ui_builder, "drawarea"));
    GdkWindow* window3 = gtk_widget_get_window(drawarea);
    cairo_region_t* cairoRegion = cairo_region_create();
    GdkDrawingContext* drawingContext = gdk_window_begin_draw_frame(window3, cairoRegion);
    cairo_t* cr = gdk_drawing_context_get_cairo_context(drawingContext);
    if (step <= 0.0) res = 0;
    if (res) {
        double* xp = calloc(1, sizeof(double));
        double* yp = calloc(1, sizeof(double));
        int xp_size = 0;
        double yn = ymax - ymin, xn = xmax - xmin;
        double xk = area_w / xn, yk = area_h / yn;
        make_points(exit_str, num_of_items, step, &xp, &yp, xmin, xmax, &xp_size);
        draw_area_clean(cr, area_h, area_w);
        fun_draw(cr, xp, yp, xp_size, xmin, xmax, ymin, ymax, area_h, area_w, xk, yk);
        xy_draw(cr, xmin, xmax, ymin, ymax, area_h, area_w, xk, yk);
        myfree(xp); myfree(yp);
    } else {
        draw_area_clean(cr, area_h, area_w);
        cairo_move_to(cr, area_w / 2 - 75, area_h / 2);
        cairo_set_font_size(cr, 50);
        cairo_set_line_width(cr, 5);
        cairo_set_source_rgb(cr, 1, 0, 0);
        cairo_show_text(cr, "ERROR");
        cairo_stroke(cr);
    }
    gdk_window_end_draw_frame(window3, drawingContext);
}

G_MODULE_EXPORT void type_res(GtkWidget *widget, GtkWidget *textbox) {
    GtkTextBuffer *buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textbox));
    GtkTextIter start, end;
    gtk_text_buffer_get_start_iter(buffer, &start);
    gtk_text_buffer_get_end_iter(buffer, &end);
    char* text = gtk_text_buffer_get_text(buffer, &start, &end, -1);
    char res_str[STACK_MAX_SIZE];
    char exit_str[STACK_MAX_SIZE][STACK_MAX_SIZE];
    int num_of_items = 0, tr_res;
    tr_res = translate(text, exit_str, &num_of_items);
    int ix = 0;
    while ((strcmp(exit_str[ix], "x")) && ix < num_of_items) ix++;
    if (tr_res) {
        if (ix == num_of_items) {
            snprintf(res_str, STACK_MAX_SIZE, "%.8lf", calc(exit_str, &num_of_items));
            int i = strlen(res_str) - 1;
            while (res_str[i] == '0') {
                res_str[i] = '\0';
                i--;
            }
            if (res_str[i] == '.') res_str[i] = '\0';
            gtk_text_buffer_set_text(gtk_text_view_get_buffer(GTK_TEXT_VIEW(textbox)), res_str, -1);
        } else {
            GtkWidget* window2 = GTK_WIDGET(gtk_builder_get_object(ui_builder, "form2"));
            gtk_widget_show_all(window2);
            draw(exit_str, &num_of_items);
        }
    } else {
        snprintf(res_str, STACK_MAX_SIZE, "error");
        gtk_text_buffer_set_text(gtk_text_view_get_buffer(GTK_TEXT_VIEW(textbox)), res_str, -1);
    }
}

double get_double_from_entry(char* w_name) {
    double res = 0;
    setlocale(LC_ALL, "en_US.utf8");
    sscanf(get_text_from_widget(w_name), "%lf", &res);
    return res;
}

void set_label_num(char* l_name, double num) {
    char text[255] = {'\0'};
    snprintf(text, sizeof(text), "%.2lf", num);
    GtkWidget* label = GTK_WIDGET(gtk_builder_get_object(ui_builder, l_name));
    gtk_label_set_text(GTK_LABEL(label), text);
}

void set_label_text(char* l_name, char* text) {
    GtkWidget* label = GTK_WIDGET(gtk_builder_get_object(ui_builder, l_name));
    gtk_label_set_text(GTK_LABEL(label), text);
}

void set_label_2_num(char* l_name, double num1, double num2) {
    char text[255] = {'\0'};
    snprintf(text, sizeof(text), "%.2lf..%.2lf", num1, num2);
    GtkWidget* label = GTK_WIDGET(gtk_builder_get_object(ui_builder, l_name));
    gtk_label_set_text(GTK_LABEL(label), text);
}

G_MODULE_EXPORT void cred_calc(GtkWidget *widget) {
    double cred_amount_ent = get_double_from_entry("cred_amount_ent"),
    cred_time_ent = get_double_from_entry("cred_time_ent"),
    cred_rate_ent = get_double_from_entry("cred_rate_ent");
    double over = 0, total = 0, calcus_ru_k = 0.000833334;
    GtkWidget* cred_annuity_rb = GTK_WIDGET(gtk_builder_get_object(ui_builder, "cred_annuity_rb"));
    GtkWidget* cred_differentiated_rb = GTK_WIDGET(gtk_builder_get_object(ui_builder,
                                                                            "cred_differentiated_rb"));
    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cred_annuity_rb))) {
        double ps = cred_rate_ent / (100 * 12), per_month = 0;
        per_month = cred_amount_ent * ps / (1 - powl(1 + ps, cred_time_ent * (-1)));
        total = per_month * cred_time_ent;
        set_label_num("cred_pay_per_month_res", per_month);
    } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cred_differentiated_rb))) {
        double per_1month = cred_amount_ent / cred_time_ent;
        double per_month = 0;
        for (int i = 0; i < cred_time_ent; i++) {
            per_month = (cred_amount_ent - per_1month * i) * calcus_ru_k * cred_rate_ent;
            total += per_1month + per_month;
        }
        set_label_2_num("cred_pay_per_month_res", per_1month + cred_amount_ent * calcus_ru_k
                        * cred_rate_ent, per_1month + per_month);
    }
    over = total - cred_amount_ent;
    set_label_num("cred_overpayment_res", over);
    set_label_num("cred_total_payment_res", total);
}

int ger_int_from_label(char* l_name) {
    int res = 0;
    GtkWidget* label = GTK_WIDGET(gtk_builder_get_object(ui_builder, l_name));
    const char* text = gtk_label_get_text(GTK_LABEL(label));
    sscanf(text, "%d", &res);
    return res;
}

void set_label_int(char* l_name, int num) {
    char text[255] = {'\0'};
    snprintf(text, sizeof(text), "%d", num);
    GtkWidget* label = GTK_WIDGET(gtk_builder_get_object(ui_builder, l_name));
    gtk_label_set_text(GTK_LABEL(label), text);
}

void del_grid_line(char* grid_name, char* rows_label_name) {
    GtkWidget* dep_rl_grid = GTK_WIDGET(gtk_builder_get_object(ui_builder, grid_name));
    int rows = ger_int_from_label(rows_label_name);
    if (rows > 0) {
        set_label_int(rows_label_name, rows - 1);
        gtk_grid_remove_row(GTK_GRID(dep_rl_grid), rows);
    }
}

G_MODULE_EXPORT void dep_ir_change(GtkWidget *widget) {
    GtkWidget* combo_box = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dep_ir_listbox"));
    GtkWidget* dep_proc_label = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dep_proc_label"));
    GtkWidget* dep_proc = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dep_proc"));
    GtkWidget* dep_ir_grid = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dep_ir_grid"));
    GtkWidget* dep_ir_plus = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dep_ir_plus"));
    GtkWidget* dep_ir_minus = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dep_ir_minus"));
    int index = gtk_combo_box_get_active(GTK_COMBO_BOX(combo_box));
    if (index == 0) {
        gtk_widget_set_visible(dep_ir_grid, 0);
        gtk_widget_set_visible(dep_ir_plus, 0);
        gtk_widget_set_visible(dep_ir_minus, 0);
        gtk_widget_set_visible(dep_proc_label, 1);
        gtk_widget_set_visible(dep_proc, 1);
    } else if (index == 1 || index == 2) {
        gtk_widget_set_visible(dep_ir_grid, 1);
        gtk_widget_set_visible(dep_ir_plus, 1);
        gtk_widget_set_visible(dep_ir_minus, 1);
        gtk_widget_set_visible(dep_proc_label, 0);
        gtk_widget_set_visible(dep_proc, 0);
    }
    int rows = ger_int_from_label("dep_ir_label");
    while (rows > 0) {
        del_grid_line("dep_ir_gri", "dep_ir_label");
        rows = ger_int_from_label("dep_ir_label");
    }
}

G_MODULE_EXPORT void dep_ir_plus_clicked_cb(GtkWidget *widget) {
    GtkWidget* combo_box = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dep_ir_listbox"));
    int index = gtk_combo_box_get_active(GTK_COMBO_BOX(combo_box));
    char text[10] = {'\0'};
    if (index == 1) {
        snprintf(text, sizeof(text), "from sum");

    } else if (index == 2) {
        snprintf(text, sizeof(text), "from day");
    }
    GtkWidget* dep_ir_grid = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dep_ir_gri"));
    GtkWidget* new_entry = gtk_entry_new();
    GtkWidget* new_label = gtk_label_new(text);
    int rows = ger_int_from_label("dep_ir_label") + 1;
    set_label_int("dep_ir_label", rows);
    gtk_grid_attach(GTK_GRID(dep_ir_grid), new_label, 0, rows, 1, 1);
    gtk_grid_attach(GTK_GRID(dep_ir_grid), new_entry, 1, rows, 1, 1);
    gtk_widget_show(dep_ir_grid);
    gtk_widget_show(new_label); gtk_widget_show(new_entry);
}

G_MODULE_EXPORT void dep_ir_minus_clicked_cb(GtkWidget *widget) {
    del_grid_line("dep_ir_gri", "dep_ir_label");
}

void new_grid_lint(char* grid_name, char* rows_label_name) {
    GtkWidget* dep_rl_grid = GTK_WIDGET(gtk_builder_get_object(ui_builder, grid_name));
    GtkWidget* new_entry = gtk_entry_new();
    GtkWidget* new_entry2 = gtk_entry_new();
    GtkWidget* new_combobox = gtk_combo_box_text_new();
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(new_combobox), NULL, "one-time");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(new_combobox), NULL, "Once a month");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(new_combobox), NULL, "Once evety 2 month");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(new_combobox), NULL, "Once a quqrter");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(new_combobox), NULL, "Semiannually");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(new_combobox), NULL, "Once a year");
    int rows = ger_int_from_label(rows_label_name) + 1;
    set_label_int(rows_label_name, rows);
    gtk_grid_attach(GTK_GRID(dep_rl_grid), new_combobox, 0, rows, 1, 1);
    gtk_grid_attach(GTK_GRID(dep_rl_grid), new_entry, 1, rows, 1, 1);
    gtk_grid_attach(GTK_GRID(dep_rl_grid), new_entry2, 2, rows, 1, 1);
    gtk_widget_show(new_combobox); gtk_widget_show(new_entry); gtk_widget_show(new_entry2);
}

G_MODULE_EXPORT void dep_rl_plus_clicked_cb(GtkWidget *widget) {
    new_grid_lint("dep_rl_grid", "dep_rl_rows_count_label");
}

G_MODULE_EXPORT void dep_rl_minus_clicked_cb(GtkWidget *widget) {
    del_grid_line("dep_rl_grid", "dep_rl_rows_count_label");
}

G_MODULE_EXPORT void dep_lopw_plus_clicked_cb(GtkWidget *widget) {
    new_grid_lint("dep_lopw_grid", "dep_lopw_rows_count_label");
}

G_MODULE_EXPORT void dep_lopw_minus_clicked_cb(GtkWidget *widget) {
    del_grid_line("dep_lopw_grid", "dep_lopw_rows_count_label");
}

int get_checkbox_status(char* name) {
    GtkWidget* checkbox = GTK_WIDGET(gtk_builder_get_object(ui_builder, name));
    return gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbox));
}

time_t str_to_date(char* str) {
    time_t now = time(NULL);
    struct tm t;
    localtime_r(&now, &t);
    sscanf(str, "%d.%d.%d", &t.tm_mday, &t.tm_mon, &t.tm_year);
    t.tm_mon = t.tm_mon - 1; t.tm_year = t.tm_year  - 1900;
    return mktime(&t);
}

double get_double_from_wentry(GtkWidget* w_name) {
    double res = 0;
    setlocale(LC_ALL, "en_US.utf8");
    sscanf((char*)gtk_entry_get_text(GTK_ENTRY(w_name)), "%lf", &res);
    return res;
}

// int days_in_this_month(time_t time) {
//     int month = localtime(&time)->tm_mon + 1;
//     return 28 + (month + month / 8) % 2  + 2 % month + 2 * (1 / month);
// }

time_t date_plus_days(time_t day, int days) {
    return day + days * 86400;
}

int day_from_time(time_t day) {
    struct tm t;
    localtime_r(&day, &t);
    return t.tm_mday;
}

int month_from_time(time_t day) {
    struct tm t;
    localtime_r(&day, &t);
    return t.tm_mon;
}

G_MODULE_EXPORT void dep_calc(GtkWidget *widget) {
    GtkWidget* dep_rl_grid = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dep_rl_grid"));
    GtkWidget* dep_lopw_grid = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dep_lopw_grid"));
    // GtkWidget* dep_ir_listbox = GTK_WIDGET(gtk_builder_get_object(ui_builder, "dep_ir_listbox"));
    GtkWidget* dep_periodicity_of_payments =
    GTK_WIDGET(gtk_builder_get_object(ui_builder, "dep_periodicity_of_payments"));
    // int dep_ir_listbox_index = gtk_combo_box_get_active(GTK_COMBO_BOX(dep_ir_listbox));
    int dep_periodicity_of_payments_index =
    gtk_combo_box_get_active(GTK_COMBO_BOX(dep_periodicity_of_payments)),
    dep_rl_rows_count_label = ger_int_from_label("dep_rl_rows_count_label"),
    dep_lopw_rows_count_label = ger_int_from_label("dep_lopw_rows_count_label");
    double dep_ic_res = 0, dep_sum = get_double_from_entry("dep_sum"),
    dep_time = get_double_from_entry("dep_time"), cb_rate = 8.5 / 100.0,
    dep_proc = get_double_from_entry("dep_proc"), dep_ta_res = 0,
    dep_tax_rate = get_double_from_entry("dep_tax_rate"),
    dep_aod_res = 0;
    double proc_per_day = dep_proc / 100.0 / 365.0;
    double temp_temp_amount = 0;

    time_t now = time(NULL);
    time_t time_t_from_table;
    GtkWidget* new_entry = NULL;

    for (int i = 1; date_plus_days(now, i) <= date_plus_days(now, dep_time); i++) {
        for (int j = 1; j <= dep_rl_rows_count_label; j++) {
            new_entry = gtk_grid_get_child_at(GTK_GRID(dep_rl_grid), 1, j);
            time_t_from_table = str_to_date((char*)gtk_entry_get_text(GTK_ENTRY(new_entry)));
            if (time_t_from_table == date_plus_days(now, i - 1)) {
                new_entry = gtk_grid_get_child_at(GTK_GRID(dep_rl_grid), 2, j);
                dep_sum += get_double_from_wentry(new_entry);
            }
        }
        for (int k = 1; k <= dep_lopw_rows_count_label; k++) {
            new_entry = gtk_grid_get_child_at(GTK_GRID(dep_lopw_grid), 1, k);
            time_t_from_table = str_to_date((char*)gtk_entry_get_text(GTK_ENTRY(new_entry)));
            if (time_t_from_table == date_plus_days(now, i - 1)) {
                new_entry = gtk_grid_get_child_at(GTK_GRID(dep_lopw_grid), 2, k);
                dep_sum -= get_double_from_wentry(new_entry);
            }
        }
        if (!(get_checkbox_status("ic_checkbox"))) {
            dep_ic_res += proc_per_day * dep_sum;
        } else {
            if (dep_periodicity_of_payments_index == 0) {
                dep_ic_res += proc_per_day * dep_sum;
                dep_sum += proc_per_day * dep_sum;
            }
            if (dep_periodicity_of_payments_index == 1) {
                temp_temp_amount += proc_per_day * dep_sum;
                if ((date_plus_days(now, i) - now) % (7 * 86400) == 0) {
                    dep_ic_res += temp_temp_amount;
                    dep_sum += temp_temp_amount;
                    temp_temp_amount = 0;
                }
            }
            if (dep_periodicity_of_payments_index == 2) {
                temp_temp_amount += proc_per_day * dep_sum;
                if (day_from_time(now) == day_from_time(date_plus_days(now, i))) {
                    dep_ic_res += temp_temp_amount;
                    dep_sum += temp_temp_amount;
                    temp_temp_amount = 0;
                }
            }
            if (dep_periodicity_of_payments_index == 3) {
                temp_temp_amount += proc_per_day * dep_sum;
                if (((month_from_time(date_plus_days(now, i)) - month_from_time(now)) % 3) == 0) {
                    dep_ic_res += temp_temp_amount;
                    dep_sum += temp_temp_amount;
                    temp_temp_amount = 0;
                }
            }
            if (dep_periodicity_of_payments_index == 4) {
                temp_temp_amount += proc_per_day * dep_sum;
                if (((month_from_time(date_plus_days(now, i)) - month_from_time(now)) % 6) == 0) {
                    dep_ic_res += temp_temp_amount;
                    dep_sum += temp_temp_amount;
                    temp_temp_amount = 0;
                }
            }
            if (dep_periodicity_of_payments_index == 5) {
                temp_temp_amount += proc_per_day * dep_sum;
                if (((month_from_time(date_plus_days(now, i)) - month_from_time(now)) % 12) == 0) {
                    dep_ic_res += temp_temp_amount;
                    dep_sum += temp_temp_amount;
                    temp_temp_amount = 0;
                }
            }
            if (dep_periodicity_of_payments_index == 6) {
                temp_temp_amount += proc_per_day * dep_sum;
                if (date_plus_days(now, i) == date_plus_days(now, dep_time)) {
                    dep_ic_res += temp_temp_amount;
                    dep_sum += temp_temp_amount;
                    temp_temp_amount = 0;
                }
            }
            if (date_plus_days(now, i) == date_plus_days(now, dep_time)) {
                dep_ic_res += temp_temp_amount;
                dep_sum += temp_temp_amount;
                temp_temp_amount = 0;
            }
        }
    }
    dep_aod_res = get_double_from_entry("dep_sum") + dep_ic_res;
    dep_ta_res = dep_ic_res - 1000000 * cb_rate;
    if (dep_ta_res > 0.0) dep_ta_res *= dep_tax_rate / 100.0;
    else
        dep_ta_res = 0;
    set_label_num("dep_ic_res", dep_ic_res);
    set_label_num("dep_ta_res", dep_ta_res);
    set_label_num("dep_aod_res", dep_aod_res);
}

G_MODULE_EXPORT void bufer_changed(GtkWidget *widget) {
    GtkWidget* textbox = GTK_WIDGET(gtk_builder_get_object(ui_builder, "textbox"));
    GtkWidget* calc_res = GTK_WIDGET(gtk_builder_get_object(ui_builder, "calc_res"));
    GtkTextBuffer *buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textbox));
    GtkTextIter start, end;
    gtk_text_buffer_get_start_iter(buffer, &start);
    gtk_text_buffer_get_end_iter(buffer, &end);
    char* text = gtk_text_buffer_get_text(buffer, &start, &end, -1);
    if (strlen(text) > 0 && text[strlen(text) - 1] == '\n') {
        text[strlen(text) - 1] = '\0';
        gtk_text_buffer_set_text(buffer, text, -1);
        type_res(calc_res, textbox);
    }
}

int main(int argc, char *argv[]) {
    gtk_init(&argc, &argv);
    GError *err = NULL;
    ui_builder = gtk_builder_new();
    if (!gtk_builder_add_from_file(ui_builder, "interface/calc_interface3.glade", &err)) {
        g_critical("Не вышло загрузить файл с UI : %s", err->message);
        g_error_free(err);
    }
    GtkWidget* window = GTK_WIDGET(gtk_builder_get_object(ui_builder, "form1"));
    gtk_builder_connect_signals(ui_builder, NULL);
    gtk_widget_show_all(window);
    gtk_main();
    return 0;
}
